package name.hanuska.crypto.jasypt.codec;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Controller
public class CodecController {

    @GetMapping("/")
    public String home(Model model, @ModelAttribute CodecBean data) {
        log.debug("GET called on /");

        if (data.getSecret() == null) {
            data.setSecret("password");
        }

        model.addAttribute("data", data);

        return "default";
    }

    @PostMapping(value = "/encode", params = "encode")
    public String encode(@ModelAttribute CodecBean data, RedirectAttributes redirectAttributes) {
        log.debug("Encoding...");

        data.setEncoded(new EncryptorBuilder().setPassword(data.getSecret()).build().encrypt(data.getContent()));
        redirectAttributes.addFlashAttribute("codecBean", data);

        return "redirect:/";
    }

    @PostMapping(value = "/encode", params = "decode")
    public String decode(@ModelAttribute CodecBean data, RedirectAttributes redirectAttributes) {
        log.debug("Decoding...");

        data.setContent(new EncryptorBuilder().setPassword(data.getSecret()).build().decrypt(data.getEncoded()));
        redirectAttributes.addFlashAttribute("codecBean", data);

        return "redirect:/";
    }

}
