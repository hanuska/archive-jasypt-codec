package name.hanuska.crypto.jasypt.codec;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Codec bean that contains the secret required for encoding and decoding.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CodecBean {

    /**
     * Secret required for encoding and decoding.
     */
    @NotNull
    private String secret;

    /**
     * Text to encode or decoded encrypted text.
     */
    private String content;

    /**
     * Encoded text or text to decode.
     */
    private String encoded;

}
