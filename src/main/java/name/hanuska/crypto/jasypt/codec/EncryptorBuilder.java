package name.hanuska.crypto.jasypt.codec;

import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.util.StringUtils;

/**
 * Builder class that creates encryptor.
 * This class allows to set alternative configurations, although it is only password for now.
 */
class EncryptorBuilder {

    private static final String DEFAULT_ALGORITHM = "PBEWithMD5AndDES";
    private static final String DEFAULT_KEY_OBTENTION_ITERATIONS = "1000";
    private static final String DEFAULT_POOL_SIZE = "1";
    private static final String DEFAULT_PROVIDER_NAME = "SunJCE";
    private static final String DEFAULT_SALT_GENERATOR_CLASS_NAME = "org.jasypt.salt.RandomSaltGenerator";
    private static final String DEFAULT_STRING_OUTPUT_TYPE = "base64";

    private String password;

    /**
     * Sets the password that configures the encryptor.
     *
     * @param password password
     * @return this builder
     */
    EncryptorBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * Creates new encryptor and configures it with given configuration.
     *
     * @return new instance of encryptor
     */
    StringEncryptor build() {

        if (StringUtils.isEmpty(password)) {
            throw new IllegalStateException("'password' must be set!");
        }

        final SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword(password);
        config.setAlgorithm(DEFAULT_ALGORITHM);
        config.setKeyObtentionIterations(DEFAULT_KEY_OBTENTION_ITERATIONS);
        config.setPoolSize(DEFAULT_POOL_SIZE);
        config.setProviderName(DEFAULT_PROVIDER_NAME);
        config.setSaltGeneratorClassName(DEFAULT_SALT_GENERATOR_CLASS_NAME);
        config.setStringOutputType(DEFAULT_STRING_OUTPUT_TYPE);

        final PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        encryptor.setConfig(config);
        return encryptor;
    }

}
